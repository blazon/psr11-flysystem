<?php

declare(strict_types=1);

namespace Blazon\PSR11FlySystem\Adapter;

use League\Flysystem\FilesystemAdapter;

interface MapperInterface
{
    public function get(string $type, array $options): FilesystemAdapter;
    public function has(string $type): bool;
    public function getFactoryClassName(string $type): ?string;
}
