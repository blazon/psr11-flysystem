<?php

declare(strict_types=1);

namespace Blazon\PSR11FlySystem\Exception;

class InvalidConfigException extends \InvalidArgumentException
{
}
